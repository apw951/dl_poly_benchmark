#!/bin/bash
mkdir data
for c in 5 6 7 8 9 10 11 12
do
	python3 gen-config.py -cells $c
	head CONFIG
	for r in $(eval echo "{1..$2}")
	do
		$1 -c CONTROL
 		mv timers run-$c-$r      	
	done
	mv run-* data/
done
