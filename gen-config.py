#!/usr/bin/env python3
import argparse
from ase.io import write, read
from ase import Atoms
from ase.lattice.cubic import FaceCenteredCubic
from io import StringIO

parser = argparse.ArgumentParser(description='Test DL_POLY correlations functions')

parser.add_argument('-cells',dest='cells',type=int,default=5,help='FCC unit cell repetition in each direction (x,y,z)')

args = parser.parse_args()

size = args.cells

atoms = FaceCenteredCubic(directions=[[1, 0, 0], [0, 1, 0], [0, 0, 1]],
                          symbol='Ar',
                          size=(size, size, size),
                          pbc=True)

write("CONFIG",atoms,format='dlp4')

f = """lj
units eV
molecular types 1
1 argon atoms
nummols       {}
atoms 1
Ar             39.95      0.0     1     0    1
finish
vdw        1
Ar      Ar      lj  0.01032   3.40
close
""".format(len(atoms))

file = open("FIELD","w")
file.write(f)
file.close()
