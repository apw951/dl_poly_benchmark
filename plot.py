import matplotlib.pyplot as plt
from ruamel.yaml import YAML
import numpy as np
from glob import glob
yaml = YAML()

runs = [f.split('-')[1:] for f in glob("data/*")]
cs = np.sort(np.unique([r[0] for r in runs]).astype(int))
rs = np.sort(np.unique([r[1] for r in runs]).astype(int))

data = yaml.load(open('data/run-5-1'))

timers = [k for k in data['timers']['Main']['Main Calc'].keys()][4:]
timers

times = dict()

for t in timers:
    times[t] = np.zeros((len(cs),len(rs),2))


for (i,c) in enumerate(cs):
    for (j,r) in enumerate(rs):
        data = yaml.load(open(f'data/run-{c}-{r}'))
        for t in timers:
            times[t][i,j,0] = data['timers']['Main']['Main Calc'][t]['Total']['average']
            times[t][i,j,1] = times[t][i,j,0]/data['timers']['Main']['Main Calc']['Total']['average']


fig, ax = plt.subplots(figsize=(10,10))
plt.rcParams.update({'font.size': 10})
N = [4*c**3 for c in cs]

markers = ['.', 'o', 'v', '^', '<', '>', '1', "s", 'p', 'P', '+', 'x', 'X']

for (i,t) in enumerate(timers):

    time = np.log10(times[t])
    mu = np.mean(time[:,:,1],axis=1)
    sig = np.std(time[:,:,1],axis=1)/np.sqrt(time.shape[1])
    
    ax.scatter(N, mu, label=t, marker=markers[i], s=90)
    ax.errorbar(N, mu, yerr=sig)

ax.legend()
ax.set_xlabel('Argon atoms')
ax.set_ylabel(f'Proportion of wall time, Log10 s')
fig.savefig('bench-prop.png')


fig, ax = plt.subplots(figsize=(10,10))
plt.rcParams.update({'font.size': 10})

for (i,t) in enumerate(timers):

    time = np.log10(times[t])
    mu = np.mean(time[:,:,0],axis=1)
    sig = np.std(time[:,:,0],axis=1)/np.sqrt(time.shape[1])
    
    ax.scatter(N, mu, label=t, marker=markers[i], s=90)
    ax.errorbar(N, mu, yerr=sig)

ax.legend()
ax.set_xlabel('Argon atoms')
ax.set_ylabel(f'Wall time, Log10 s')
fig.savefig('bench.png')

benchmark = dict()

benchmark['atoms'] = N

for t in timers:
    time = times[t]
    mu = np.mean(time[:,:,1],axis=1).tolist()
    sig = np.std(time[:,:,1],axis=1)/np.sqrt(time.shape[1]).tolist()
    
    data = np.zeros((len(mu), 3))
    data[:, 0] = N
    data[:, 1] = mu
    data[:, 2] = sig

    np.savetxt("".join(t.split(" "))+".csv", data)
